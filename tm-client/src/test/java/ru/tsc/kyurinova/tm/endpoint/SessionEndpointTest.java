package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kyurinova.tm.marker.SoapCategory;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    public SessionEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Test
    @Category(SoapCategory.class)
    public void openSessionTest() {
        Assert.assertNotNull(sessionEndpoint.openSession("test", "test"));
    }

    @Test
    @Category(SoapCategory.class)
    public void closeSessionTest() {
        @NotNull final SessionDTO session = sessionEndpoint.openSession("test", "test");
        sessionEndpoint.closeSession(session);
    }

}
