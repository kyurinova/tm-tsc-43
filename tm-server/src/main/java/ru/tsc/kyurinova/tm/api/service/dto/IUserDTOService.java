package ru.tsc.kyurinova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.service.model.IService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserDTOService extends IDTOService<UserDTO> {

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    void isLoginExists(@Nullable String login);

    void isEmailExists(@Nullable String email);

    void addAll(@NotNull List<UserDTO> users);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    void removeByLogin(@Nullable String login);

    @NotNull UserDTO createAdmin(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    UserDTO updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @Nullable
    UserDTO lockUserByLogin(@Nullable String login);

    @Nullable
    UserDTO unlockUserByLogin(@Nullable String login);

    void remove(@Nullable UserDTO entity);

    @NotNull
    List<UserDTO> findAll();

    void clear();

    @Nullable
    UserDTO findById(@Nullable String id);

    @NotNull
    UserDTO findByIndex(@Nullable Integer index);

    void removeById(@Nullable String id);

    void removeByIndex(@Nullable Integer index);

    boolean existsById(@Nullable String id);

    boolean existsByIndex(@NotNull Integer index);

    int getSize();

}
