package ru.tsc.kyurinova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.model.ITaskRepository;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    protected EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull String userId, @NotNull Task task) {
        task.setUser(entityManager.find(User.class, userId));
        entityManager.persist(task);
    }

    @Override
    public @Nullable Task findByName(@NotNull String userId, @NotNull String name) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.name = :name and m.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeByName(@NotNull String userId, @NotNull String name) {
        entityManager.remove(findByName(userId, name));
    }

    @Override
    public void startById(@NotNull String userId, @NotNull String id) {
        final Task task = findById(userId, id);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        entityManager.merge(task);
    }

    @Override
    public void startByIndex(@NotNull String userId, @NotNull Integer index) {
        final Task task = findByIndex(userId, index);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        entityManager.merge(task);
    }

    @Override
    public void startByName(@NotNull String userId, @NotNull String name) {
        final Task task = findByName(userId, name);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        entityManager.merge(task);
    }

    @Override
    public void finishById(@NotNull String userId, @NotNull String id) {
        final Task task = findById(userId, id);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        entityManager.merge(task);
    }

    @Override
    public void finishByIndex(@NotNull String userId, @NotNull Integer index) {
        final Task task = findByIndex(userId, index);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        entityManager.merge(task);
    }

    @Override
    public void finishByName(@NotNull String userId, @NotNull String name) {
        final Task task = findByName(userId, name);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        entityManager.merge(task);
    }

    @Override
    public void update(@NotNull String userId, @NotNull Task task) {
        final String taskUserId = task.getUser().getId();
        if (userId.equals(taskUserId))
            entityManager.merge(task);
    }

    @Override
    public void changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) {
        final Task task = findById(userId, id);
        task.setStatus(status);
        entityManager.merge(task);
    }

    @Override
    public void changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) {
        final Task task = findByIndex(userId, index);
        task.setStatus(status);
        entityManager.merge(task);
    }

    @Override
    public void changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        final Task task = findByName(userId, name);
        task.setStatus(status);
        entityManager.merge(task);
    }

    @Override
    public void bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        final Task task = findById(userId, taskId);
        task.setProject(entityManager.find(Project.class, projectId));
        entityManager.merge(task);
    }

    @Override
    public void unbindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        final Task task = findById(userId, taskId);
        task.setProject(null);
        entityManager.merge(task);
    }

    @Override
    public @Nullable Task findByProjectAndTaskId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId and m.project.id = :projectId and m.id = :taskId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setParameter("taskId", taskId)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.project.id = :projectId and m.user.id = :userId";
        entityManager.createQuery(jpql).
                setParameter("projectId", projectId).
                setParameter("userId", userId).
                executeUpdate();

    }

    @Override
    public @NotNull List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId and m.project.id = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void remove(@NotNull String userId, @NotNull Task task) {
        @Nullable final String taskUserId = task.getUser().getId();
        if (userId.equals(taskUserId))
            entityManager.remove(task);
    }

    @Override
    public @NotNull List<Task> findAll() {
        return entityManager.createQuery("SELECT m FROM Task m", Task.class)
                .getResultList();
    }

    @Override
    public @NotNull List<Task> findAllUserId(@NotNull String userId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void clearUserId(@NotNull String userId) {
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.user.id = :userId";
        entityManager.createQuery(jpql).
                setParameter("userId", userId).
                executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Task").
                executeUpdate();
    }

    @Override
    public @Nullable Task findById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId  and m.id = :id";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Task findByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @Nullable final Task task = findById(userId, id);
        entityManager.remove(task);
    }
}
