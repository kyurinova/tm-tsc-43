package ru.tsc.kyurinova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_user")
public class User extends AbstractModel {

    @NotNull
    @Column
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column
    private String email;

    @Nullable
    @Column(name = "fst_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "mid_name")
    private String middleName;

    @NotNull
    @Column
    private Role role = Role.USER;

    @Nullable
    @Column
    private String locked = "N";

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();


}
