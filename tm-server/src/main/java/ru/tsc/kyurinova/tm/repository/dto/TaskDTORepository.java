package ru.tsc.kyurinova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.enumerated.Status;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class TaskDTORepository implements ITaskDTORepository {

    protected EntityManager entityManager;

    public TaskDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull String userId, @NotNull TaskDTO task) {
        task.setUserId(userId);
        entityManager.persist(task);
    }

    @Override
    public @Nullable TaskDTO findByName(@NotNull String userId, @NotNull String name) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.name = :name and m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeByName(@NotNull String userId, @NotNull String name) {
        entityManager.remove(findByName(userId, name));
    }

    @Override
    public void startById(@NotNull String userId, @NotNull String id) {
        final TaskDTO taskDTO = findById(userId, id);
        taskDTO.setStatus(Status.IN_PROGRESS);
        taskDTO.setStartDate(new Date());
        entityManager.merge(taskDTO);
    }

    @Override
    public void startByIndex(@NotNull String userId, @NotNull Integer index) {
        final TaskDTO taskDTO = findByIndex(userId, index);
        taskDTO.setStatus(Status.IN_PROGRESS);
        taskDTO.setStartDate(new Date());
        entityManager.merge(taskDTO);
    }

    @Override
    public void startByName(@NotNull String userId, @NotNull String name) {
        final TaskDTO taskDTO = findByName(userId, name);
        taskDTO.setStatus(Status.IN_PROGRESS);
        taskDTO.setStartDate(new Date());
        entityManager.merge(taskDTO);
    }

    @Override
    public void finishById(@NotNull String userId, @NotNull String id) {
        final TaskDTO taskDTO = findById(userId, id);
        taskDTO.setStatus(Status.COMPLETED);
        taskDTO.setFinishDate(new Date());
        entityManager.merge(taskDTO);
    }

    @Override
    public void finishByIndex(@NotNull String userId, @NotNull Integer index) {
        final TaskDTO taskDTO = findByIndex(userId, index);
        taskDTO.setStatus(Status.COMPLETED);
        taskDTO.setFinishDate(new Date());
        entityManager.merge(taskDTO);
    }

    @Override
    public void finishByName(@NotNull String userId, @NotNull String name) {
        final TaskDTO taskDTO = findByName(userId, name);
        taskDTO.setStatus(Status.COMPLETED);
        taskDTO.setFinishDate(new Date());
        entityManager.merge(taskDTO);
    }

    @Override
    public void update(@NotNull String userId, @NotNull TaskDTO task) {
        final String taskUserId = task.getUserId();
        if (userId.equals(taskUserId))
            entityManager.merge(task);
    }

    @Override
    public void changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) {
        final TaskDTO taskDTO = findById(userId, id);
        taskDTO.setStatus(status);
        entityManager.merge(taskDTO);
    }

    @Override
    public void changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) {
        final TaskDTO taskDTO = findByIndex(userId, index);
        taskDTO.setStatus(status);
        entityManager.merge(taskDTO);
    }

    @Override
    public void changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        final TaskDTO taskDTO = findByName(userId, name);
        taskDTO.setStatus(status);
        entityManager.merge(taskDTO);
    }

    @Override
    public void bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        final TaskDTO taskDTO = findById(userId, taskId);
        taskDTO.setProjectId(projectId);
        entityManager.merge(taskDTO);
    }

    @Override
    public void unbindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        final TaskDTO taskDTO = findById(userId, taskId);
        taskDTO.setProjectId(null);
        entityManager.merge(taskDTO);
    }

    @Override
    public @Nullable TaskDTO findByProjectAndTaskId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId and m.projectId = :projectId and m.id = :taskId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setParameter("taskId", taskId)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.projectId = :projectId and m.userId = :userId";
        entityManager.createQuery(jpql).
                setParameter("projectId", projectId).
                setParameter("userId", userId).
                executeUpdate();

    }

    @Override
    public @NotNull List<TaskDTO> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId and m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void remove(@NotNull String userId, @NotNull TaskDTO task) {
        @Nullable final String taskUserId = task.getUserId();
        if (userId.equals(taskUserId))
            entityManager.remove(task);
    }

    @Override
    public @NotNull List<TaskDTO> findAll() {
        return entityManager.createQuery("SELECT m FROM TaskDTO m", TaskDTO.class)
                .getResultList();
    }

    @Override
    public @NotNull List<TaskDTO> findAllUserId(@NotNull String userId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void clearUserId(@NotNull String userId) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql).
                setParameter("userId", userId).
                executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM TaskDTO").
                executeUpdate();
    }

    @Override
    public @Nullable TaskDTO findById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId  and m.id = :id";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable TaskDTO findByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @Nullable final TaskDTO taskDTO = findById(userId, id);
        entityManager.remove(taskDTO);
    }
}
