package ru.tsc.kyurinova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void removeProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    ProjectDTO entity
    );

    @WebMethod
    @NotNull List<ProjectDTO> findAllProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    );

    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProjectsSorted(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "sort", partName = "sort")
                    String sort
    );

    @WebMethod
    void clearProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    );

    @WebMethod
    @Nullable ProjectDTO findByIdProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull ProjectDTO findByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void removeByIdProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    void removeByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    boolean existsByIdProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    boolean existsByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void createProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void createProjectDescr(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    @NotNull ProjectDTO findByNameProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void removeByNameProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void updateByIdProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    void updateByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    void startByIdProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    void startByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void startByNameProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void finishByIdProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    void finishByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void finishByNameProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void changeStatusByIdProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    void changeStatusByIndexProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    void changeStatusByNameProject(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    void removeProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    ProjectDTO entity
    );

    @WebMethod
    @NotNull List<ProjectDTO> findAllProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    );

    @WebMethod
    void clearProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    );

    @WebMethod
    @Nullable ProjectDTO findByIdProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull ProjectDTO findByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void removeByIdProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    void removeByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    boolean existsByIdProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    boolean existsByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    );
}
