package ru.tsc.kyurinova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User findByEmail(@Nullable String email);

    void isLoginExists(@Nullable String login);

    void isEmailExists(@Nullable String email);

    void addAll(@NotNull List<User> users);

    @Nullable
    User findByLogin(@Nullable String login);

    void removeByLogin(@Nullable String login);

    @NotNull User createAdmin(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    User updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @Nullable
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User unlockUserByLogin(@Nullable String login);

    void remove(@Nullable User entity);

    @NotNull
    List<User> findAll();

    void clear();

    @Nullable
    User findById(@Nullable String id);

    @NotNull
    User findByIndex(@Nullable Integer index);

    void removeById(@Nullable String id);

    void removeByIndex(@Nullable Integer index);

    boolean existsById(@Nullable String id);

    boolean existsByIndex(@NotNull Integer index);

    int getSize();

}
