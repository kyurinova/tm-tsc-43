package ru.tsc.kyurinova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.enumerated.Status;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class ProjectDTORepository implements IProjectDTORepository {

    protected EntityManager entityManager;

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull String userId, @NotNull ProjectDTO project) {
        project.setUserId(userId);
        entityManager.persist(project);
    }

    @Override
    public @Nullable ProjectDTO findByName(@NotNull String userId, @NotNull String name) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.name = :name and m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeByName(@NotNull String userId, @NotNull String name) {
        entityManager.remove(findByName(userId, name));
    }

    @Override
    public void startById(@NotNull String userId, @NotNull String id) {
        final ProjectDTO projectDTO = findById(userId, id);
        projectDTO.setStatus(Status.IN_PROGRESS);
        projectDTO.setStartDate(new Date());
        entityManager.merge(projectDTO);
    }

    @Override
    public void startByIndex(@NotNull String userId, @NotNull Integer index) {
        final ProjectDTO projectDTO = findByIndex(userId, index);
        projectDTO.setStatus(Status.IN_PROGRESS);
        projectDTO.setStartDate(new Date());
        entityManager.merge(projectDTO);
    }

    @Override
    public void startByName(@NotNull String userId, @NotNull String name) {
        final ProjectDTO projectDTO = findByName(userId, name);
        projectDTO.setStatus(Status.IN_PROGRESS);
        projectDTO.setStartDate(new Date());
        entityManager.merge(projectDTO);
    }

    @Override
    public void finishById(@NotNull String userId, @NotNull String id) {
        final ProjectDTO projectDTO = findById(userId, id);
        projectDTO.setStatus(Status.COMPLETED);
        projectDTO.setFinishDate(new Date());
        entityManager.merge(projectDTO);
    }

    @Override
    public void finishByIndex(@NotNull String userId, @NotNull Integer index) {
        final ProjectDTO projectDTO = findByIndex(userId, index);
        projectDTO.setStatus(Status.COMPLETED);
        projectDTO.setFinishDate(new Date());
        entityManager.merge(projectDTO);
    }

    @Override
    public void finishByName(@NotNull String userId, @NotNull String name) {
        final ProjectDTO projectDTO = findByName(userId, name);
        projectDTO.setStatus(Status.COMPLETED);
        projectDTO.setFinishDate(new Date());
        entityManager.merge(projectDTO);
    }

    @Override
    public void update(@NotNull String userId, @NotNull ProjectDTO project) {
        final String projectUserId = project.getUserId();
        if (userId.equals(projectUserId))
            entityManager.merge(project);
    }

    @Override
    public void changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) {
        final ProjectDTO projectDTO = findById(userId, id);
        projectDTO.setStatus(status);
        entityManager.merge(projectDTO);
    }

    @Override
    public void changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) {
        final ProjectDTO projectDTO = findByIndex(userId, index);
        projectDTO.setStatus(status);
        entityManager.merge(projectDTO);
    }

    @Override
    public void changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        final ProjectDTO projectDTO = findByName(userId, name);
        projectDTO.setStatus(status);
        entityManager.merge(projectDTO);
    }

    @Override
    public void remove(@NotNull String userId, @NotNull ProjectDTO project) {
        @Nullable final String projectUserId = project.getUserId();
        if (userId.equals(projectUserId))
            entityManager.remove(project);
    }

    @Override
    public @NotNull List<ProjectDTO> findAll() {
        return entityManager.createQuery("SELECT m FROM ProjectDTO m", ProjectDTO.class)
                .getResultList();
    }

    @Override
    public @NotNull List<ProjectDTO> findAllUserId(@NotNull String userId) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM ProjectDTO").
                executeUpdate();
    }

    @Override
    public void clearUserId(@NotNull String userId) {
        @NotNull final String jpql = "DELETE FROM ProjectDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public @Nullable ProjectDTO findById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId and m.id = :id";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable ProjectDTO findByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @Nullable final ProjectDTO projectDTO = findById(userId, id);
        entityManager.remove(projectDTO);
    }
}
