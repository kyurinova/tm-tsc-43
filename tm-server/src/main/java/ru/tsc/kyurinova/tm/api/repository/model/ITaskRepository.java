package ru.tsc.kyurinova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(
            @NotNull final String userId,
            @NotNull final Task task
    );

    @Nullable
    Task findByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void removeByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void startById(
            @NotNull final String userId,
            @NotNull final String id
    );

    void startByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    );

    void startByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void finishById(
            @NotNull final String userId,
            @NotNull final String id
    );

    void finishByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    );

    void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void update(
            @NotNull final String userId,
            @NotNull final Task task
    );

    void changeStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    );

    void changeStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final Status status
    );

    void changeStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    );

    void bindTaskToProjectById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    );

    void unbindTaskToProjectById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    );

    @Nullable
    Task findByProjectAndTaskId(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    );

    void removeAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    @NotNull
    List<Task> findAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    void remove(
            @NotNull final String userId,
            @NotNull final Task task
    );

    @NotNull
    List<Task> findAll(
    );

    @NotNull
    List<Task> findAllUserId(
            @NotNull final String userId
    );

    void clearUserId(
            @NotNull final String userId
    );

    void clear(
    );

    @Nullable
    Task findById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Nullable
    Task findByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    );

    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

}
