package ru.tsc.kyurinova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kyurinova.tm.api.service.IServiceLocator;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    TaskDTO entity
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull List<TaskDTO> findAllTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO findByIdTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull TaskDTO findByIndexTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIdTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIndexTaskUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().existsByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @WebParam(name = "entity", partName = "entity")
                    TaskDTO entity
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull List<TaskDTO> findAllTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull List<TaskDTO> findAllTaskSorted(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "sort", partName = "sort") final String sort
    ) {
        System.out.println("SIGNATURE");
        System.out.println(session.getSignature());
        System.out.println("USERID");
        System.out.println(session.getUserId());
        System.out.println("TIME");
        System.out.println(session.getTimestamp());
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO findByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull TaskDTO findByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().existsByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createTaskDescr(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull TaskDTO findByNameTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByNameTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void updateByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void updateByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void startByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void startByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void startByNameTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void finishByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void finishByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void finishByNameTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void changeStatusByIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void changeStatusByIndexTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void changeStatusByNameTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO findByProjectAndTaskIdTask(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByProjectAndTaskId(session.getUserId(), projectId, taskId);
    }

}
