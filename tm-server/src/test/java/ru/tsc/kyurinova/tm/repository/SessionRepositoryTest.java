package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.repository.dto.SessionDTORepository;
import ru.tsc.kyurinova.tm.repository.dto.UserDTORepository;
import ru.tsc.kyurinova.tm.service.ConnectionService;
import ru.tsc.kyurinova.tm.service.PropertyService;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class SessionRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private SessionDTO session;

    @NotNull
    private String sessionId;

    @NotNull
    private final String userId;

    public EntityManager GetEntityManager() {
        return connectionService.getEntityManager();
    }

    public SessionRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("test");
        userId = user.getId();
        @NotNull final String password = "test";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
        userRepository.add(user);
        entityManager.getTransaction().commit();
    }

    @Before
    public void before() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        entityManager.getTransaction().begin();
        session = new SessionDTO();
        sessionId = session.getId();
        session.setUserId(userId);
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        entityManager.getTransaction().commit();
    }

    @Test
    public void existsSessionTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.findById(sessionId));
    }

    @Test
    public void removeSessionByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        Assert.assertNotNull(session);
        Assert.assertNotNull(sessionId);
        entityManager.getTransaction().begin();
        sessionRepository.removeById(sessionId);
        entityManager.getTransaction().commit();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        entityManager.getTransaction().begin();
        sessionRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
